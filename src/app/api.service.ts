import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService {

  constructor(
    private http: Http
  ) { }

  getPosts() {
    let Url = "https://jsonplaceholder.typicode.com/posts";
    return this.http.get(Url)
      .map((response) => response.json())
  }

  getSpecificPosts(value) {
    let Url = "https://jsonplaceholder.typicode.com/posts/" + value;
    return this.http.get(Url)
      .map((response) => response.json())
  }


}
