import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ApiService } from './api.service';
import { SinglePostComponent } from './single-post/single-post.component';
import { HttpModule } from '@angular/http';
import { TitleFilterPipe } from './title-filter.pipe';
import { HttpClientModule } from '@angular/common/http';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'post/:id/:jobtitles', component: SinglePostComponent },

];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SinglePostComponent,
    TitleFilterPipe
  ],
  imports: [

    // BrowserModule
    BrowserModule.withServerTransition({ appId: 'my-universal-app' }),
    HttpModule,
    HttpClientModule,
    BrowserTransferStateModule,
    //RouterModule.forRoot(appRoutes, {initialNavigation:'enabled' })
	RouterModule.forRoot(appRoutes)

  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
