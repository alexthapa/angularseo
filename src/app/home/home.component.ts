import { Component, OnInit } from '@angular/core';
import { Title, Meta, TransferState, makeStateKey } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

// make state key in state to store users
const STATE_KEY_USERS = makeStateKey('PostData');
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    public PostData: any = [];

    constructor(
        private http: HttpClient,
        private state: TransferState
    ) { }

    ngOnInit() {

        this.PostData = this.state.get(STATE_KEY_USERS, <any>[]);


        if (this.PostData.length == 0) {
            this.http.get('https://jsonplaceholder.typicode.com/posts')
                .subscribe((PostData) => {
                    this.PostData = PostData;
                    this.state.set(STATE_KEY_USERS, <any>PostData);
                }, (err) => {
                    console.log(err);
                });
        }
    }
}
