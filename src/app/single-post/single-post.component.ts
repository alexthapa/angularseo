import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Meta, Title } from "@angular/platform-browser";

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {
	
	jobtitle;


  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
	meta: Meta, title: Title
  ) {
	  
	  
    this.route.params.subscribe(params => {
      this.postId = +params['id']; // (+) converts string 'id' to a number
	  this.jobtitle = params['jobtitles']; 
      this.getSingleData(this.postId)
	  console.log(this.jobtitle);

    });
	
	
	   title.setTitle(this.jobtitle);

    meta.addTags([
      { name: 'author',   content: 'Coursetro.com'},
      { name: 'keywords', content: 'angular seo, angular 4 universal, etc'},
      { name: 'description', content: 'This is my Angular SEO-based App, enjoy it!' }
    ]);

	  }

  postId;
  singleData = {
    id: null,
    title: null,
    body: null
  };

  ngOnInit() {

  }

  getSingleData(id) {
    this.apiService.getSpecificPosts(id).subscribe(
      (reponse) => {
        this.singleData = reponse;
      }
    )
  }


}
