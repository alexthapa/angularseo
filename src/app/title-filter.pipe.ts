import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleFilter'
})
export class TitleFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
	  
	  let data = value.toString();
	  data = data.replace(/\s/g,'-')
    return data;
  }

}
